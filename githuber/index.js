import { AppRegistry } from 'react-native'
import { name as appName } from './app.json'
import App from './src'

if (__DEV__) {
  import('react-devtools')
  import('./src/config/ReactotronConfig').then(() =>
    console.log('Reactotron Configu3red')
  )
}

AppRegistry.registerComponent(appName, () => App)
