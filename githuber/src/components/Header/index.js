import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  AsyncStorage,
  TouchableOpacity,
  StatusBar
} from 'react-native'
import { withNavigation } from 'react-navigation'

import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome'

function Header ({ title, navigation }) {
  const signOut = async () => {
    await AsyncStorage.removeItem('@githuber:username')
    navigation.navigate('Welcome')
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle='dark-content' />

      <View style={styles.left} />
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity onPress={signOut}>
        <Icon name='exchange' size={16} style={styles.icon} />
      </TouchableOpacity>
    </View>
  )
}

Header.propTypes = {
  title: PropTypes.string.isRequired
}

export default withNavigation(Header)
