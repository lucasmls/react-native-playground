import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, AsyncStorage, ActivityIndicator, FlatList } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './styles'
import api from '~/services/api'

import Header from '~/components/Header'
import RepositoryItem from './RepositoryItem'

class Repositories extends Component {
  state = {
    repositories: [],
    loading: true,
    refreshing: false
  }

  renderListItem = ({ item }) => <RepositoryItem repository={item} />

  renderList = () => {
    const { repositories, refreshing } = this.state

    return (
      <FlatList
        data={repositories}
        keyExtractor={item => String(item.id)}
        renderItem={this.renderListItem}
        onRefresh={this.fetchRepositories}
        refreshing={refreshing}
      />
    )
  }

  fetchRepositories = async () => {
    this.setState({ refreshing: true })
    const username = await AsyncStorage.getItem('@githuber:username')
    const { data } = await api.get(`/users/${username}/repos`)
    this.setState({
      repositories: [...data],
      loading: false,
      refreshing: false
    })
  }

  componentDidMount () {
    this.fetchRepositories()
  }

  render () {
    const { loading } = this.state

    return (
      <View style={styles.container}>
        <Header title='Repositórios' />
        {loading ? (
          <ActivityIndicator styles={styles.loading} />
        ) : (
          this.renderList()
        )}
      </View>
    )
  }
}

const TabIcon = ({ tintColor }) => (
  <Icon name='list-alt' size={20} color={tintColor} />
)

TabIcon.propTypes = {
  tintColor: PropTypes.string.isRequred
}

Repositories.navigationOptions = {
  tabBarIcon: TabIcon
}

export default Repositories
