import React, { useState } from 'react'
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  AsyncStorage
} from 'react-native'

import styles from './styles'
import api from '~/services/api'

function Welcome ({ navigation }) {
  const [username, setUsername] = useState('')
  const [loading, setLoading] = useState(false)
  const [hasError, setHasError] = useState(false)

  const saveUser = async githubUsername => {
    await AsyncStorage.setItem('@githuber:username', githubUsername)
  }

  const checkUserExistence = async githubUsername => {
    const user = await api.get(`/users/${githubUsername}`)
    return user
  }

  const signIn = async () => {
    setLoading(true)

    try {
      await checkUserExistence(username)
      await saveUser(username)

      navigation.navigate('App')
    } catch (error) {
      setLoading(false)
      setHasError(true)
      console.tron.log('Usuário inexistente')
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle='light-content' />
      <Text style={styles.title}>Bem-vindo!</Text>
      <Text style={styles.text}>
        Para continuar precisamos que você nos informe o seu usuário no Github.
      </Text>

      {hasError && <Text style={styles.error}>Usuário inexiste.</Text>}

      <View style={styles.form}>
        <TextInput
          style={styles.input}
          autoCapitalize='none'
          autoCorrect={false}
          placeholder='Digite seu usuário'
          underlineColorAndroid='transparent'
          onChangeText={text => setUsername(text)}
        />

        <TouchableOpacity style={styles.button} onPress={signIn}>
          {loading ? (
            <ActivityIndicator size='small' color='#FFF' />
          ) : (
            <Text style={styles.buttonText}>Prosseguir</Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Welcome
