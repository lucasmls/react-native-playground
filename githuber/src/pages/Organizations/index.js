import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, AsyncStorage, ActivityIndicator, FlatList } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './styles'
import api from '~/services/api'

import Header from '~/components/Header'
import OrganizationItem from './OrganizationItem'

class Organizations extends Component {
  state = {
    organizations: [],
    loading: true,
    refreshing: false
  }

  renderListItem = ({ item }) => <OrganizationItem organization={item} />

  renderList = () => {
    const { organizations, refreshing } = this.state
    console.tron.log(organizations)

    return (
      <FlatList
        data={organizations}
        keyExtractor={item => String(item.id)}
        renderItem={this.renderListItem}
        numColumns={2}
        columnWrapperStyle={styles.collumnWrapper}
        onRefresh={this.fetchOrganizations}
        refreshing={refreshing}
      />
    )
  }

  fetchOrganizations = async () => {
    this.setState({ refreshing: true })
    const username = await AsyncStorage.getItem('@githuber:username')
    const { data } = await api.get(`/users/${username}/orgs`)
    console.tron.log(data)
    this.setState({
      organizations: [...data],
      loading: false,
      refreshing: false
    })
  }

  componentDidMount () {
    this.fetchOrganizations()
  }

  render () {
    const { loading } = this.state

    return (
      <View style={styles.container}>
        <Header title='Organizações' />
        {loading ? (
          <ActivityIndicator styles={styles.loading} />
        ) : (
          this.renderList()
        )}
      </View>
    )
  }
}

const TabIcon = ({ tintColor }) => (
  <Icon name='building' size={20} color={tintColor} />
)

TabIcon.propTypes = {
  tintColor: PropTypes.string.isRequred
}

Organizations.navigationOptions = {
  tabBarIcon: TabIcon
}

export default Organizations
