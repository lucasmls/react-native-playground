import {
  createAppContainer,
  createSwitchNavigator,
  createBottomTabNavigator
} from 'react-navigation'

import { colors } from '~/styles'

import Welcome from '~/pages/Welcome'
import Repositories from '~/pages/Repositories'
import Organizations from '~/pages/Organizations'

const Routes = (userLogged = false) =>
  createAppContainer(
    createSwitchNavigator(
      {
        Welcome,
        App: createBottomTabNavigator(
          {
            Repositories,
            Organizations
          },
          {
            tabBarOptions: {
              showIcon: true,
              showLabel: false,
              activeTintColor: colors.white,
              inactiveTintColor: colors.inactiveTintColor,
              style: {
                backgroundColor: colors.secondary
              }
            }
          }
        )
      },
      { initialRouteName: userLogged ? 'App' : 'Welcome' }
    )
  )

export default Routes
