/**
 * Types
 */
export const Types = {
  FETCH_ISSUES: 'issues/FETCH_ISSUES',
  FETCH_ISSUES_SUCCESS: 'issues/FETCH_ISSUES_SUCCESS',
  FETCH_ISSUES_FAILURE: 'issues/FETCH_ISSUES_FAILURE'
}

/**
 * Actions
 */
export const Creators = {
  fetchIssues: () => ({
    type: Types.FETCH_ISSUES
  }),

  fetchIssuesSuccess: issues => ({
    type: Types.FETCH_ISSUES_SUCCESS,
    payload: { issues }
  }),

  fetchIssuesFailure: error => ({
    type: Types.FETCH_ISSUES_FAILURE,
    payload: { error }
  })
}

/**
 * Reducer
 */
export const INITIAL_STATE = {
  isFetching: false,
  issues: [],
  error: ''
}

export function issuesReducer (state, action) {
  switch (action.type) {
    case Types.FETCH_ISSUES:
      return { ...state, isFetching: true }

    case Types.FETCH_ISSUES_SUCCESS:
      return {
        issues: [...action.payload.issues],
        isFetching: false,
        error: ''
      }

    case Types.FETCH_ISSUES_FAILURE:
      return { ...state, error: action.payload.error, isFetching: false }

    default:
      return state
  }
}
