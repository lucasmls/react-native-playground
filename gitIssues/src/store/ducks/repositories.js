/**
 * Types
 */
export const Types = {
  FETCH_REPOSITORY: 'repositories/FETCH_REPOSITORY',
  FETCH_REPOSITORY_SUCCESS: 'repositories/FETCH_REPOSITORY_SUCCESS',
  FETCH_REPOSITORY_FAILURE: 'repositories/FETCH_REPOSITORY_FAILURE'
}

/**
 * Actions
 */
export const Creators = {
  fetchRepository: () => ({
    type: Types.FETCH_REPOSITORY
  }),

  fetchRepositorySuccess: repository => ({
    type: Types.FETCH_REPOSITORY_SUCCESS,
    payload: { repository }
  }),

  fetchRepositoryFailure: error => ({
    type: Types.FETCH_REPOSITORY_FAILURE,
    payload: { error }
  })
}

/**
 * Reducer
 */
export const INITIAL_STATE = {
  isFetching: false,
  repositories: [],
  error: ''
}

export function repositoriesReducer (state, action) {
  switch (action.type) {
    case Types.FETCH_REPOSITORY:
      return { ...state, isFetching: true }

    case Types.FETCH_REPOSITORY_SUCCESS:
      return {
        repositories: [...state.repositories, action.payload.repository],
        isFetching: false,
        error: ''
      }

    case Types.FETCH_REPOSITORY_FAILURE:
      return { ...state, error: action.payload.error, isFetching: false }

    default:
      return state
  }
}
