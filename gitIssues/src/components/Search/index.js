import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './styles'

function Search ({ handleSubmit, isFetching, error }) {
  const [repository, setRepository] = useState('facebook/react')

  return (
    <>
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          autoCapitalize='none'
          autoCorrect={false}
          placeholder='Adicionar novo repositório'
          underlineColorAndroid='transparent'
          value={repository}
          onChangeText={text => setRepository(text)}
        />

        <TouchableOpacity
          onPress={() => handleSubmit(repository)}
          disabled={isFetching}
        >
          {isFetching ? (
            <ActivityIndicator />
          ) : (
            <Icon style={styles.icon} size={16} name='plus' />
          )}
        </TouchableOpacity>
      </View>
      {!!error && <Text style={styles.errorMessage}>{error}</Text>}
    </>
  )
}

Search.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isFetching: PropTypes.bool,
  error: PropTypes.string
}

export default Search
