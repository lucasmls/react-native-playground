import { StyleSheet } from 'react-native'
import { colors, metrics } from '~/styles'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: metrics.basePadding / 2,
    marginVertical: metrics.baseMargin / 2,
    borderBottomWidth: 1,
    borderBottomColor: colors.regular
  },

  input: {
    backgroundColor: colors.white,
    borderRadius: metrics.baseRadius,
    height: 40,
    paddingHorizontal: metrics.basePadding / 2,
    flex: 1
  },

  icon: {
    marginLeft: metrics.baseMargin,
    fontWeight: '700'
  },

  errorMessage: {
    color: colors.danger,
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center'
  }
})

export default styles
