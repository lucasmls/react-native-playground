import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, TouchableOpacity } from 'react-native'

import styles from './styles'

const FilterTab = ({ filterOptions, activeFilter, updateFilter }) => (
  <View style={styles.container}>
    {filterOptions.map(option => (
      <TouchableOpacity
        key={option}
        style={styles.filterItem}
        onPress={() => updateFilter(option)}
      >
        <Text
          style={[
            styles.filterText,
            activeFilter === option ? styles.active : ''
          ]}
        >
          {option}
        </Text>
      </TouchableOpacity>
    ))}
  </View>
)

FilterTab.propTypes = {
  filterOptions: PropTypes.arrayOf(PropTypes.string).isRequired,
  updateFilter: PropTypes.func,
  activeFilter: PropTypes.string
}

export default FilterTab
