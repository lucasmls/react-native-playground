import { StyleSheet } from 'react-native'
import { colors, metrics } from '~/styles'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: metrics.baseMargin,
    backgroundColor: colors.lighter,
    borderRadius: metrics.baseRadius
  },

  filterItem: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: metrics.basePadding / 2.5
  },

  filterText: {
    color: colors.regular
  },

  active: {
    color: colors.dark,
    fontWeight: 'bold'
  }
})

export default styles
