import { StyleSheet } from 'react-native'
import { colors, metrics } from '~/styles'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    alignItems: 'center',
    padding: metrics.basePadding / 2,
    borderRadius: metrics.baseRadius,
    marginTop: metrics.baseMargin
  },

  image: {
    width: 50,
    height: 50
  },

  round: {
    borderRadius: 25
  },

  content: {
    flex: 1,
    paddingHorizontal: metrics.basePadding
  },

  icon: {
    color: colors.regular,
    marginLeft: metrics.baseMargin
  },

  title: {
    color: colors.secundary,
    fontWeight: 'bold',
    fontSize: 17
  },

  subtitle: {
    color: colors.regular,
    fontWeight: 'bold',
    fontSize: 14
  }
})

export default styles
