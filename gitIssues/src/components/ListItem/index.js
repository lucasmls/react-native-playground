import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'

import styles from './styles'

const ListItem = ({ avatar, title, subtitle, onPress, rounded }) => (
  <TouchableOpacity onPress={onPress} style={styles.container}>
    <Image
      style={[styles.image, rounded && styles.round]}
      source={{
        uri: avatar
      }}
    />
    <View style={styles.content}>
      <Text numberOfLines={1} ellipsizeMode='tail' style={styles.title}>
        {title}
      </Text>
      <Text style={styles.subtitle}>{subtitle}</Text>
    </View>

    <Text style={styles.icon}>
      <Icon name='right' />
    </Text>
  </TouchableOpacity>
)

ListItem.defaultProps = {
  rounded: false
}

ListItem.propTypes = {
  avatar: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  rounded: PropTypes.bool
}

export default ListItem
