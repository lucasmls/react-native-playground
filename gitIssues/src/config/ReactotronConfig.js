import Reactotron from 'reactotron-react-native'

const tron = Reactotron.configure()
  .useReactNative()
  .connect()

console.tron = tron
tron.clear()

module.exports = tron
