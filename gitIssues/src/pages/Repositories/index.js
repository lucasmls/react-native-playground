import React, { useEffect, useReducer } from 'react'
import { View, AsyncStorage, FlatList } from 'react-native'

import ListItem from '~/components/ListItem'
import Search from '~/components/Search'

import styles from './styles'
import api from '~/services/api'

import {
  INITIAL_STATE as repositoriesInitialState,
  repositoriesReducer,
  Creators as RepositoryCreators
} from '~/store/ducks/repositories'

function Repositories ({ navigation }) {
  const [state, dispatch] = useReducer(
    repositoriesReducer,
    repositoriesInitialState
  )

  /**
   * @param {String} repository
   * @description Performs the request to the Github API to get the repository and saves it on the async storage and useReducer start
   */
  const fetchRepository = async repository => {
    dispatch(RepositoryCreators.fetchRepository())
    try {
      const { data } = await api.get(`/repos/${repository}`)

      dispatch(RepositoryCreators.fetchRepositorySuccess(data))

      await AsyncStorage.setItem(
        '@git-issues/repositories',
        JSON.stringify([...state.repositories, data])
      )
    } catch (error) {
      dispatch(
        RepositoryCreators.fetchRepositoryFailure('Repositório inexistente')
      )
    }
  }

  /**
   * @description It retrieves the repositories from the async storage and updates the useReducer start
   */
  const retrieveRepositoriesFromStorage = async () => {
    const storageRepositories = JSON.parse(
      (await AsyncStorage.getItem('@git-issues/repositories')) || [] //  In case don't have any repository on storage, it saves an empty array
    )

    storageRepositories.forEach(repository =>
      dispatch(RepositoryCreators.fetchRepositorySuccess(repository))
    )
  }

  /**
   * @description On componentDidMount, it triggers the function to retrieve the repositories from async storage
   */
  useEffect(() => {
    // AsyncStorage.clear()
    retrieveRepositoriesFromStorage()
  }, [])

  return (
    <View style={styles.container}>
      <Search
        handleSubmit={fetchRepository}
        isFetching={state.isFetching}
        error={state.error}
      />

      <FlatList
        data={state.repositories}
        keyExtractor={repository => String(repository.id)}
        renderItem={({ item: repository }) => (
          <ListItem
            key={repository.id}
            repositoryId={repository.id}
            avatar={repository.owner.avatar_url}
            title={repository.name}
            subtitle={repository.owner.login}
            onPress={() =>
              navigation.navigate('Issues', {
                repositoryId: repository.id,
                repositoryName: repository.name
              })
            }
          />
        )}
      />
    </View>
  )
}

Repositories.navigationOptions = {
  title: 'Gitissues'
}

export default Repositories
