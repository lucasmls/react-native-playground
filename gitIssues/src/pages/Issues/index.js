import React, { useState, useEffect, useReducer } from 'react'
import { View, Text, FlatList, ActivityIndicator, Linking } from 'react-native'

import ListItem from '~/components/ListItem'
import FilterTab from '~/components/FilterTab'
import styles from './styles'
import api from '~/services/api'

import {
  INITIAL_STATE as issuesInitialState,
  issuesReducer,
  Creators as IssuesCreators
} from '~/store/ducks/issues'

function Issues ({ navigation }) {
  const [state, dispatch] = useReducer(issuesReducer, issuesInitialState)

  const [filter, setFilter] = useState({
    options: ['Todas', 'Abertas', 'Fechadas'],
    active: 'Todas'
  })

  const fetchIssues = async (filter = 'all') => {
    dispatch(IssuesCreators.fetchIssues())
    try {
      const repositoryId = navigation.getParam('repositoryId')
      const { data } = await api.get(
        `/repositories/${repositoryId}/issues?state=${filter}`
      )

      dispatch(IssuesCreators.fetchIssuesSuccess(data))
    } catch (error) {
      dispatch(
        IssuesCreators.fetchIssuesFailure(
          'Erro ao buscar as issues deste repositório'
        )
      )
    }
  }

  const updateFilter = activeFilter => {
    setFilter({ ...filter, active: activeFilter })

    const filterMap = {
      Todas: 'all',
      Abertas: 'open',
      Fechadas: 'closed'
    }

    fetchIssues(filterMap[activeFilter])
  }

  useEffect(() => {
    fetchIssues()
  }, [])

  return (
    <View style={styles.container}>
      <FilterTab
        filterOptions={filter.options}
        activeFilter={filter.active}
        updateFilter={active => updateFilter(active)}
      />

      {state.isFetching ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={state.issues}
          keyExtractor={issue => String(issue.id)}
          onRefresh={fetchIssues}
          refreshing={state.isFetching}
          renderItem={({ item: issue }) => (
            <ListItem
              key={issue.id}
              title={issue.title}
              subtitle={issue.user.login}
              avatar={issue.user.avatar_url}
              onPress={() => Linking.openURL(issue.html_url)}
              rounded
            />
          )}
        />
      )}

      {!!state.error && <Text style={styles.errorMessage}>{state.error}</Text>}
    </View>
  )
}
Issues.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('repositoryName')
})

export default Issues
