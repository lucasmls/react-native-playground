import React, { Component } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native'

import Post from '~/components/Post'

export default class App extends Component {
  state = {
    posts: [
      {
        id: 1,
        title: 'Título 01',
        author: 'Lucas Mendes',
        content:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit.Quibusdam, exercitationem!'
      },
      {
        id: 2,
        title: 'Título 02',
        author: 'Laisla',
        content:
          'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et dolor incidunt accusamus iusto dolorum.'
      },
      {
        id: 3,
        title: 'Título 03',
        author: 'Daniel Lima',
        content:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit omnis sed ipsa autem voluptatibus dolorum repudiandae!'
      },
      {
        id: 4,
        title: 'Título 04',
        author: 'Laislinha',
        content:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur eum eos, assumenda perferendis accusamus reiciendis laudantium non. Hic, totam harum!'
      },
      {
        id: 5,
        title: 'Título 01',
        author: 'Lucas Mendes',
        content:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit.Quibusdam, exercitationem!'
      },
      {
        id: 6,
        title: 'Título 02',
        author: 'Laisla',
        content:
          'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et dolor incidunt accusamus iusto dolorum.'
      },
      {
        id: 7,
        title: 'Título 03',
        author: 'Daniel Lima',
        content:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit omnis sed ipsa autem voluptatibus dolorum repudiandae!'
      },
      {
        id: 8,
        title: 'Título 04',
        author: 'Laislinha',
        content:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur eum eos, assumenda perferendis accusamus reiciendis laudantium non. Hic, totam harum!'
      }
    ]
  }

  render () {
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.state.posts.map(post => (
            <Post
              key={post.id}
              title={post.title}
              author={post.author}
              content={post.content}
            />
          ))}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#df7d7b',
    paddingTop: 50,
    paddingHorizontal: 15,
    margin: 0
  }
})
