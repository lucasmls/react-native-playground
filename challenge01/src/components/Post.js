import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View } from 'react-native'

const Post = ({ title, author, content }) => (
  <View style={styles.post}>
    <View style={styles.postHeader}>
      <Text style={styles.postHeaderTitle}>{title}</Text>
      <Text style={styles.postHeaderSubtitle}>{author}</Text>
    </View>
    <Text style={styles.postContent}>{content}</Text>
  </View>
)

Post.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
}

const styles = StyleSheet.create({
  post: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 15,
    margin: 0,
    borderRadius: 5,
    marginBottom: 15
  },

  postHeader: {
    paddingBottom: 10,
    borderBottomColor: '#c3c3c3',
    borderBottomWidth: 1
  },

  postHeaderTitle: {
    color: '#525659',
    fontSize: 15,
    fontWeight: 'bold',
    marginBottom: 3
  },

  postHeaderSubtitle: {
    color: '#c3c3c3',
    fontSize: 14,
    fontWeight: '500'
  },

  postContent: {
    paddingVertical: 10,
    color: '#525659',
    fontSize: 13
  }
})

export default Post
