const rootImportPugin = () => [
  'babel-plugin-root-import',
  {
    rootPathSuffix: 'src'
  }
]

module.exports = function (api) {
  api.cache(true)

  const presets = ['module:metro-react-native-babel-preset']
  const plugins = [rootImportPugin()]

  if (process.env['ENV'] === 'prod') {
    plugins.push(rootImportPugin())
  }

  return {
    presets,
    plugins
  }
}
