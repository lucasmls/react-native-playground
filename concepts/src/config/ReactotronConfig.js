import Reactotron from 'reactotron-react-native'

// Reactotron.configure({ host: 'IP_DA_MAQUINA' }) ||| Caso o reactotron não funcione no USB/Android,
const tron = Reactotron.configure()
  .useReactNative()
  .connect()

console.tron = tron
tron.clear()

module.exports = tron
