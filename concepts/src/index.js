import React, { Component } from 'react'
import { StyleSheet, Text, View, Button, Platform } from 'react-native'

import Todo from '~/components/Todo'

export default class App extends Component {
  state = {
    todos: [
      { id: 1, text: 'Fazer café!' },
      { id: 2, text: 'Estudar o goNative!' }
    ]
  }

  addTodo = () => {
    this.setState(state => {
      return {
        todos: [
          ...state.todos,
          { id: Date.now(), text: 'Estudar mais e mais javascript...' }
        ]
      }
    })

    console.tron.log('Console.log pelo Reactotron')
  }

  /**
   * Não devemos realizar alteração em nenhum dos métodos abaixo...
   */
  static getDerivedStateFromProps (nextProps, prevState) {
    return { text: nextProps.text }
  }

  /**
   * Este método apenas não permite renderizar o componente,
   * mas o estado pode continuar sendo atualizado em "background"
   * da mesma forma...
   */
  shouldComponentUpdate (nextProps, nextState) {
    return this.state.todos.length < 6
  }

  componentDidUpdate (prevProps, prevState) {}

  componentWillUnmount () {}

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to goNative!</Text>
        {this.state.todos.map(todo => (
          <Todo text={todo.text} key={todo.id} />
        ))}
        <Button onPress={this.addTodo} title='Adicionar todo' />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    ...Platform.select({
      ios: {
        backgroundColor: '#F5FCFF'
      },
      android: {
        backgroundColor: '#F00'
      }
    })
  }
})
